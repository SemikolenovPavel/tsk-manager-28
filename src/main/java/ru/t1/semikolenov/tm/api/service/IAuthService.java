package ru.t1.semikolenov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.model.User;

public interface IAuthService {

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @Nullable String email);

    @NotNull
    User getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void checkRoles(@Nullable Role[] roles);

}
